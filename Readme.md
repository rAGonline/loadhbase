##Program to Load record into HBase

#Command to create Hbase table

hbase(main):001:0> create 'review', 'review details', 'review data', 'supplementary data'

#command to insert data
hbase(main):004:0> put 'review','1','review details:reviewerID','XXX9999'
hbase(main):004:0>put 'review','1','review details:asin','8888'


#command to run the program
hduser@WubbyDesk:/usr/local/LoadHbase/src$
java -classpath "/usr/local/hbase/hbase-1.4.1/lib/*:rs.jar:/usr/local/lib/*" com.loadhbase.Loadhbase

#Older versions of run command
hduser@WubbyDesk:/usr/local/LoadHbase/src$ 
java -cp "../resources/hbase-client-1.4.1.jar:../resources/hbase-common-1.4.1.jar:rs.jar:../resources/hadoop-common-2.9.0.jar:../resources/json-simple-1.1.jar" com.loadhbase.Loadhbase

