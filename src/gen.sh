rm com/loadhbase/*.class
rm rs.jar

javac -cp "../resources/hbase-client-1.4.1.jar:../resources/hbase-common-1.4.1.jar:../resources/hadoop-common-2.9.0.jar:../resources/json-simple-1.1.jar" com/loadhbase/Loadhbase.java

jar cf rs.jar com/loadhbase/*.class

